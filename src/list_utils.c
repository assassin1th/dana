#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "rib_utils.h"
#include "list.h"
// Вставка элемента в начало списка, так проще и есть меньше времени
Node *insert(Node *head, Rib *rib) { 
	Rib *new_rib = create_rib(rib->start, rib->end);
	Node *node = (Node *) calloc(1, sizeof(Node));

	node->rib = new_rib;
	node->next = head;
	
	return node;
}

// Удаляет узел списка вместе со значением в нем
Node *delete(Node *head, Rib *rib) {
	if (!strcmp(head->rib->start->name, rib->start->name) && 
		!strcmp(head->rib->end->name, rib->end->name)) {
		Node *tmp = head->next;
		free_rib(head->rib);
		free(head);
		return tmp;
	} else {
		head->next = delete(head->next, rib);
	}
	return head;
}

// Освобождает память из под списка
void free_list(Node *head) {
	if (head) {
		free_list(head->next);
		free_rib(head->rib);
		free(head);
	}
}
// Выводит список, опять таки ничего хитрого
void print_list(Node *head) {
	printf("{");
	while (head) {
		print_rib(head->rib);
		if ((head = head->next) != NULL) {
			printf(", ");
		}
	}
	printf("}");
}

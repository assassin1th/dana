#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "graph_utils.h"
#include "vertex_utils.h"
#include "rib_utils.h"
// инициализация графа
Graph *init_graph(Graph *graph);
// вывод инструкции
void print_instruction(void);

int main(int argc, char *argv[]) {
	int size = 1; // Первичная величина массива указателей
	Vertex **arr = (Vertex **) calloc(size, sizeof(Vertex*)); // выделение памяти под массив указателей
	
	char word[32];
	int n_vertex = 0; // начальное кол-во вершин
	
	printf("Введите имена вершин, через enter: ");
	while (scanf("%s", word) != -1) {
		if (search_vertex(arr, n_vertex, word)) {
			// проверка на то чтобы вершины не повторялись
			printf("Вершина с именем %s уже существует\n", word);
		} else {
			if (n_vertex >= size) {
				// Если случилось так что не осталось места увеличиваем размер в 2 раза
				size = size << 1; // это увеличит size в 2 раза
				arr = (Vertex **) realloc(arr, sizeof(Vertex *) * size); // Перевыделяем память
			}
			arr[n_vertex] = create_vertex(word); // сохраняем в массиве указателей указатель на созданную вершину
			n_vertex++; // увеличиваем счетчик вершин на 1
		}
		printf("Введите имена вершин, через enter: ");
	}
	printf("\n");
	
	Graph *graph = create_graph(arr, n_vertex, 0); // Создаем граф передавая в функцию массив указателей кол-во вершин и флаги
	graph = init_graph(graph); // инициализируем граф

	int answer = 0;
	while (answer >= 0) {
		switch (answer) {
			case 0 :
				// Вывести инструкцию
				print_instruction();
				break;
			case 1 : {
				// Удалить ребро
				/*
				** Тут нет никакой магии, просто в точности такой же алгоритм как и
				** в функции инициализации графа, только всего лишь функция 
				** вставки ребра заменена на функцию удаления ребра
				*/
				char start[32];
				char end[32];
				printf("Введите вершины, удаляемого ребра (начало конец): ");
				scanf("%s %s", start, end);
				Vertex *v_start;
				Vertex *v_end;
				if ((v_start = search_vertex(graph->vertex, graph->n_vertex, start)) == NULL) {
					printf("Вершины с именем %s не существует\n", start);
				} else if ((v_end = search_vertex(graph->vertex, graph->n_vertex, end)) == NULL) {
					printf("Вершины с именем %s не существует\n", end);
				} else {
					Rib *rib = create_rib(v_start, v_end);
					graph = delete_rib(graph, rib);
					free_rib(rib);
				}
				break;
			}
			case 2 :
				// Сортировать граф
				sort_graph(graph);
				printf("Готово\n");
				break;
			case 3 : {
				/*
				FILE *fp = fopen("graph.dot", "w");
				fprintf(fp, "graph name {\n");
				for (int i = 0; i < n_vertex; i++) {
					fprintf(fp, "%s;\n", arr[i]->name);
				}
				for (Node *p = graph->rib_list; p != NULL; p = p->next) {
					fprintf(fp, "%s--%s;\n", p->rib->start->name, p->rib->end->name);
				}
				fprintf(fp, "}\n");
				fclose(fp);
				system("dot -Tpng graph.dot -o graph.png");
				system("gwenview graph.png");
				*/
				print_graph(graph);
				break;
			}
			default :
				break;
		}
		printf("Введите действие (0 - инструкция): ");
		scanf("%d", &answer);
	}
	// Далее просто освобождение памяти из под всего, что осталось
	for (int i = 0; i < n_vertex; i++) {
		free_vertex(arr[i]);
	}

	free(arr);
	free_graph(graph);

	return 0;
}
// Инициализирует граф
Graph *init_graph(Graph *graph) {
	char name[32];
	printf("Введите имя графа: ");
	scanf("%s", name); // получаем имя графа
	graph->name = strdup(name); // сохраняем его
	char start[32]; // Для имени вершины начала ребра
	char end[32]; // Для имени вершины конца ребра
	printf("Введите ребра (начало конец): ");
	while (scanf("%s %s", start, end) > 0) {
		Vertex *v_start; // Указатели на вершины, куда мы сохраняем найденные вершины
		Vertex *v_end;
		if ((v_start = search_vertex(graph->vertex, graph->n_vertex, start)) == NULL) {
			// если не найдена вершина начала ребра
			printf("Вершины с именем %s не существует\n", start);
		} else if ((v_end = search_vertex(graph->vertex, graph->n_vertex, end)) == NULL) {
			// если не найдена вершина конца ребра
			printf("Вершины с именем %s не существует\n", end);
		} else {
			// если все найдено создаем временное ребро
			Rib *rib = create_rib(v_start, v_end);
			graph = add_rib(graph, rib);
			free_rib(rib); // Можем спокойно освобождать память потому что add_rib созадет копию и ее сохраняет
		}
		printf("Введите ребра (начало конец): ");
	}
	printf("\n");
	return graph; // Возвращаем указатель на граф
}

void print_instruction(void) {
	printf(	"0 - инструкция\n"
			"1 - удаление ребра\n"
			"2 - сортировка графа\n"
			"3 - вывод графа\n"
			"-1 - выход\n");
}

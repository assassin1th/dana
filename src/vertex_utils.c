#include <stdlib.h>
#include <string.h>
#include "vertex.h"
// Создает вершину из имени
Vertex *create_vertex(char *name) {
	Vertex *vertex = (Vertex *) calloc(1, sizeof(Vertex)); // Выделяем память
	
	vertex->name = strdup(name); // выделяем память под имя вершины и копируем туда это имя
	vertex->degree = 0; // степень по умолчанию равна 0
	
	return vertex; // Возвращаем новую вершину
}
// Ищет в массиве указателей на вершины нужную вершину по имени
Vertex *search_vertex(Vertex **arr, int n, char *name) { 
	for (int i = 0; i < n; i++) {
		if (!strcmp(arr[i]->name, name)) { // если strcmp вернет ноль возвращаем указатель на элемент
			return arr[i];
		}
	}
	return NULL; // возвращаем NULL если ничего не нашли
}
// Освобождает память из-под вершины
void free_vertex(Vertex *vertex) {
	free(vertex->name);
	free(vertex);
}

#include "vertex.h"
#define swap(a, b) {  /* просто меняет элементы местами*/			\
	__auto_type tmp = a;  /*__auto_type - макрос компилятора*/		\
	a = b;															\
	b = tmp;														\
}

void bouble_sort(Vertex **v, int n) { // принимает на вход массив указателей на вершины и длину массива
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < i; j++) {
			if (v[i]->degree < v[j]->degree)  // если степень i-й вершины меньше
				swap(v[i], v[j]);			  // степени j-й вершины
		}
	}	
}

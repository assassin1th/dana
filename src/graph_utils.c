#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "graph.h"
#include "flag.h"
#include "bouble_sort.h"
#include "list_utils.h"

// Создание графа из массива указателей на вершины кол-ва вершин и флагов
Graph *create_graph(Vertex **vertex, int n_vertex, int flags) {
	Graph *graph_ptr = (Graph *) calloc(1, sizeof(Graph));
	graph_ptr->vertex = (Vertex **) calloc(n_vertex, sizeof(Vertex *));
	
	for (int i = 0; i < n_vertex; i++) {
		graph_ptr->vertex[i] = vertex[i];
	}
	
	graph_ptr->rib_list = NULL;
	graph_ptr->n_vertex = n_vertex;
	set_flag(graph_ptr->flags, flags);

	return graph_ptr;
}
// освобождение памяти из под графа
void free_graph(Graph *graph_ptr) {
	free_list(graph_ptr->rib_list);
	free(graph_ptr->vertex);
	free(graph_ptr);
}
// Сортировка графа
Graph *sort_graph(Graph *graph_ptr) { // Принимает на вход граф
	bouble_sort(graph_ptr->vertex, graph_ptr->n_vertex); // передает сортировке массив указателей
	return graph_ptr; // возвращает модифицированный граф
}

Graph *add_rib(Graph *graph_ptr, Rib *rib) { // Добавляет ребро путем вставки в список
	graph_ptr->rib_list = insert(graph_ptr->rib_list, rib);
	rib->start->degree++; // увеличивает степени соот вершин
	rib->end->degree++;
	return graph_ptr;
}

Graph *add_vertex(Graph *graph_ptr, Vertex *vertex) { // Добавляет вершину занося ее в массив указателей на вершины
	graph_ptr->vertex[graph_ptr->n_vertex++] = vertex; // и увеличивает количество вершин на 1
	graph_ptr->vertex = (Vertex **) realloc(graph_ptr->vertex, sizeof(Vertex *) * 	\
											(graph_ptr->n_vertex));
	return graph_ptr;
}
// Делает абсолютно противоположные вещи по отношению к add_rib
Graph *delete_rib(Graph *graph_ptr, Rib *rib) {
	graph_ptr->rib_list = delete(graph_ptr->rib_list, rib); 
	rib->start->degree--;
	rib->end->degree--;
	return graph_ptr;
}

void print_graph(Graph *graph_ptr) { // Просто выводит элементы графа, ничего хитрого
	printf("%s = {{", graph_ptr->name);
	for (int i = 0; i < graph_ptr->n_vertex; i++) {
		printf(	(i == graph_ptr->n_vertex - 1) ? "%s}" : "%s, ",					\
				graph_ptr->vertex[i]->name);
	}
	printf(", ");
	print_list(graph_ptr->rib_list);
	printf("}\n");
}


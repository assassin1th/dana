#include <stdlib.h>
#include <stdio.h>
#include "rib.h"
// Создает ребро из 2х вершин (начала и конца)
Rib *create_rib(Vertex *start, Vertex *end) {
	Rib *rib = (Rib *) calloc(1, sizeof(Rib)); //выделяем память
	
	rib->start = start;	//присваиваем адреса соот переменным
	rib->end = end;

	return rib;	// Возвращаем ребро
}
// Освобождает память из-под ребра
void free_rib(Rib *rib) { 
	free(rib);
}
// Печатает ребро, в виде сортированного множества (не помню как эта хрень называется)
void print_rib(Rib *rib) {
	printf("<%s, %s>", rib->start->name, rib->end->name);
}

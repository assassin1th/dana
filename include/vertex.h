#ifndef PROJECT_INCLUDE_VERTEX_H_
#define PROJECT_INCLUDE_VERTEX_H_

typedef struct vertex {
	unsigned int degree;
	char *name;
} Vertex;

#endif  // PROJECT_INCLUDE_VERTEX_H_

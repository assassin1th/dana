#ifndef PROJECT_INCLUDE_LIST_UTILS_H_
#define PROJECT_INCLUDE_LIST_UTILS_H_

#include "list.h"

Node *insert(Node *head, Rib *rib);
Node *delete(Node *head, Rib *rib);
void free_list(Node *head);
void print_list(Node *head);

#endif  // PROJECT_INCLUDE_LIST_UTILS_H_

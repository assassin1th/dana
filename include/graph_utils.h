#ifndef PROJECT_INCLUDE_GRAPH_UTILS_H_
#define PROJECT_INCLUDE_GRAPH_UTILS_H_

#include "graph.h"

Graph *delete_rib(Graph *, Rib *);
Graph *create_graph(Vertex **, int,  int);
Graph *sort_graph(Graph *);
Graph *add_vertex(Graph *, Vertex *);
Graph *add_rib(Graph *, Rib *);
void free_graph(Graph *);
void print_graph(Graph *);

#endif // PROJECT_INCLUDE_GRAPH_UTILS_H_

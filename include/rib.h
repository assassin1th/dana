#ifndef PROJECT_INCLUDE_RIB_H_
#define PROJECT_INCLUDE_RIB_H_

#include "vertex.h"

typedef struct rib {
	Vertex *start;
	Vertex *end;
} Rib;

#endif  // PROJECT_INCLUDE_RIB_H_
#ifndef PROJECT_INCLUDE_LIST_H_
#define PROJECT_INCLUDE_LIST_H_

#include "rib.h"

typedef struct node {
	Rib *rib;
	struct node *next;
} Node;

#endif  // PROJECT_INCLUDE_LIST_H_

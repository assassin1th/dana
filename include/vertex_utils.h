#ifndef PROJECT_INCLUDE_VERTEX_UTILS_H_
#define PROJECT_INCLUDE_VERTEX_UTILS_H_

#include "vertex.h"

Vertex *create_vertex(char *name);
Vertex *search_vertex(Vertex **, int, char *);
void free_vertex(Vertex *);

#endif  // PROJECT_INCLUDE_VERTEX_UTILS_H_
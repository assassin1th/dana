#ifndef PROJECT_INCLUDE_GRAPH_H_
#define PROJECT_INCLUDE_GRAPH_H_

#include "vertex.h"
#include "rib.h"
#include "list.h"

typedef struct graph {
	unsigned int flags;
	unsigned int n_vertex;
	char *name;
	Vertex **vertex;
	Node *rib_list;
} Graph;


#endif  // PROJECT_INCLUDE_GRAPH_H_

#ifndef PROJECT_INCLUDE_RIB_UTILS_H_
#define PROJECT_INCLUDE_RIB_UTILS_H_

#include "rib.h"

void free_rib(Rib *);
Rib *create_rib(Vertex *, Vertex *);
void print_rib(Rib *);

#endif  // PROJECT_INCLUDE_RIB_UTILS_H_

#ifndef PROJECT_INCLUDE_FLAG_H_
#define PROJECT_INCLUDE_FLAG_H_

#define set_flag(flags, flag) (flags |= flag)
#define unset_flag(flags, flag) (flags &= ~flag)
#define check_flag(flags, flag) (flags & flag)

#endif  // PROJECT_INCLUDE_FLAG_H_

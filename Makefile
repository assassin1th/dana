CC = gcc
SRC_DIR = src/
HDRS_DIR = include/
FLAGS = -I$(HDRS_DIR)
TARGET = main
DEP =	$(SRC_DIR)$(TARGET).o			\
		$(SRC_DIR)graph_utils.o			\
		$(SRC_DIR)bouble_sort.o			\
		$(SRC_DIR)list_utils.o			\
		$(SRC_DIR)rib_utils.o			\
		$(SRC_DIR)vertex_utils.o		\
		$(SRC_DIR)graph_utils.o			

$(SRC_DIR)%.o : $(SRC_DIR)%.c
	$(CC) -c $< -o $@ $(FLAGS)
$(TARGET) : $(DEP)
	$(CC) $^ -o $@
.PHONY : clean delete test

delete :
	rm $(TARGET)
clean :
	rm $(SRC_DIR)*.o